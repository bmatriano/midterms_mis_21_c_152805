class Budget	
		def initialize(initial_balance)
			@initial_balance = initial_balance
			@record_expenses = Array.new()
			@record_income = Array.new()
			puts "Initial Balance: #{'%.02f'%initial_balance}"
			@months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
		end
		def add_expense (item,amount,date)
			date_split = date.split("-")
			month = @months[date_split[1].to_i - 1]
			puts "#{item} : An amount of #{'%.02f'%amount} was deducted from the balance on #{month} #{date_split[2]},#{date_split[0]} "
			@record_expenses << Record.new(date_split[0],date_split[1],date_split[2],amount)
			@initial_balance -= amount
		end
		def add_income (item,amount,date)
			date_split = date.split("-")
			month = @months[date_split[1].to_i - 1]
			puts "#{item} : An amount of #{amount} was added to the balance on #{month} #{date_split[2]},#{date_split[0]}"
			@record_income << Record.new(date_split[0], date_split[1] ,date_split[2],amount)
			@initial_balance += amount
		end
		def current
			puts "Balance: #{'%.02f'% @initial_balance}"
		end
		def show_all
			sum = 0.0
			@record_expenses.each do |item|
				puts item.month
				puts item.amount.class
				if item.month=="06" 
					puts "#{item.year} #{item.month} #{'%.02f'% item.amount}"
					sum+=item.amount
				end
				end
			puts sum	
		end
		def monthly_report (month,year)
		sum_expenses=0
		sum_income=0
			check = @months[month - 1]
			@record_expenses.each do |item|
				if item.year.to_i==year
					if item.month.to_i==month
						sum_expenses = sum_expenses + item.amount.to_f
					else
					end
				else
				end
			end
			@record_income.each do |item|
				if item.year.to_i==year
					if item.month.to_i==month.to_i
						sum_income += item.amount.to_f
					else
					end
				else
				end	
			end
			puts "====================================="
			puts "MONTHLY REPORT: #{month} #{year}"
			puts "Total Expense: #{'%.02f'% sum_expenses}"
			puts "Total Income: #{'%.02f'% sum_income}"
			puts "====================================="
		end
end

class Record
	attr_accessor :year, :month, :date, :amount
	def initialize(year,month,date,amount)
		@year = year
		@month = month
		@date = date
		@amount = amount
	end
end