require './budget'

budget = Budget.new(50000.00)
budget.add_expense("Petron", 5000.00, "2017-06-15")
budget.add_income("Work", 6000.00, "2017-06-26")
budget.add_expense("National Bookstore", 2500.00, "2017-06-27")
budget.add_expense("Gonzaga", 200.00, "2017-06-27")
budget.add_income("Freelance", 10000.00, "2017-07-10")
budget.current
budget.monthly_report(6,2017)
budget.add_expense("Nintendo Switch", 19000.00, "2017-07-15")
budget.add_expense("Samsung S8 Edge", 39300.00, "2017-07-20")
budget.monthly_report(7,2017)
budget.current