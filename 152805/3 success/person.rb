class Person
	attr_accessor :name, :success_point
	def initialize(name)
		@name = name
		@success_point = 0.0
	end
	
	def to_s
		puts name
	end
	def retrieve
	if @success_point>0
		puts "#{name} #{success_point}"
	else
	end
	end
	def expected_score(another_person)
		1/(1+10**((another_person.success_point-@success_point)/400))
	end
	
	def new_success_points(score, another_person)
		if (@success_point + 32*(score-expected_score(another_person))) >= 0
			@success_point= @success_point + 32*(score-expected_score(another_person))
		else
			@success_point = 0
		end
	end
	def self.update_success_points(winner, loser)
	winner.new_success_points(1,loser)
	loser.new_success_points(0,winner)
	end
end