require './to_do_list'

todo = ToDoList.new

puts "================ \n"

todo.add_task("Clean room")
todo.add_task("Submit MIS21 HW")
todo.add_task("Study for MIS21 midterms")
todo.add_task("Study for Stat midterms")
todo.add_task("Submit advisement form")
todo.add_task("Consult for final project")

puts "================ \n"

todo.view_pending_tasks

puts "================ \n"

todo.accomplish_task(0)
todo.accomplish_task(2)
todo.accomplish_task(2)

puts "================ \n"

todo.view_accomplished_tasks