class ToDoList
	def initialize
		@current = Array.new()
		@accomplished = Array.new()
	end
	def add_task(task_name)
		@current.push(task_name)
		puts "#{task_name} has been added to your task list"
	end
	def view_pending_tasks
		x=1
		puts "Pending tasks:"
		@current.each do |task|
			puts "(#{x}) #{task}"
			x=x.next
		end
	end
	def accomplish_task (index)
		@accomplished << @current[index]
		puts "#{@current[index]} has been marked as accomplished"
		@current.delete_at(index)
	end
	def view_accomplished_tasks
		x=1
		puts "Accomplished tasks:"
		@accomplished.each do |task|
			puts "(#{x}) #{task}"
			x=x.next
		end
	end
end